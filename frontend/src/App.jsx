import { useState } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <button onClick={() => setCount(count + 1)}
        >
          Click
        </button>
        <div>
          =====================
        </div>
        <div>
          {count}
        </div>
    </div>
    </>
  )
}

export default App
