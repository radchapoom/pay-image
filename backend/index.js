// imports
const express = require("express");
const app = express()
require('dotenv').config()
const cors = require("cors");
const bodyParser = require("body-parser");
//prisma
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

// specify the port from our enviroment variable
const PORT = process.env.PORT || 8080;
app.use(cors());
app.use(express.json());


// use the body-parser middleware to parse JSON and URL-encoded data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// connect database

// routes
app.get("/", async (req, res) => {
  res.send("Hello World!");
});

app.get('/image/:imageId', (req, res) => {
  res.status(200).send(`Image ${req.params.imageId}`)
})

app.post('/create/image', async (req, res) => {
  let r = (Math.random() + 1).toString(36).substring(7);
  const user = await prisma.user.create({
    data: {
      name: r,
      email: `${r}@prisma.com`,
      posts: {
        create: {
          title: r,
          body: 'Lots of really interesting stuff',
          slug: 'my-first-post',
        },
      },
    },
  })
  res.json(user)
});

app.put('/update/:id', (req, res) => {
  res.status(200).json({
    'image': req.params.id
  })
});

app.listen(PORT, () => {
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`);
});
